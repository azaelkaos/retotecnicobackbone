<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Zipcodes;

class ZipcodesController extends Controller
{
    public function zipcodes(Request $request) {
		$aux=null;
		if(is_numeric($request->zipcode)){
			$aux=Zipcodes::select('id','idEstado','estado','idMunicipio','municipio','ciudad','zona','cp','asentamiento','tipo')->where('cp', $request->zipcode)->get()->toArray();
		}else{
			$whereConfdition[]=['estado','=',$request->zipcode];
			if(!empty($request->localidad)){
				$whereConfdition[]=['ciudad','=',$request->localidad];
			}
			
			$aux=Zipcodes::select('id','idEstado','estado','idMunicipio','municipio','ciudad','zona','cp','asentamiento','tipo')->where($whereConfdition)->get()->toArray();
		}
		if(empty($aux)){
			abort(404);
			die();
		}
		$settlements= array_map(function ($sub_array) {
			return [
				"key"=>$sub_array['id'],
				"name"=> $sub_array['asentamiento'],
				"zone_type"=> $sub_array['zona'],
				"settlement_type"=> [
					"name"=> $sub_array['tipo']
				]
			];
		},$aux);

		return response()->json([
			"zip_code"=>$aux[0]['cp'],
			"locality"=>$aux[0]['ciudad'],
			"federal_entity"=> [
				"key"=> $aux[0]['idEstado'],
				"name"=> $aux[0]['estado'],
				"code"=> null
			],
			"settlements"=>$settlements
		]);
    }
}
