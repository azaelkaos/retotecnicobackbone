# retoTecnicoBackBone

Reto Tecnico
Backend Developer

BackBone Systems

## Importante

Se ha creado esta API para tratar de replicar https://jobs.backbonesystems.io/api/zip-codes/77500

Se ha tomado la bdd de https://github.com/redrbrt/sepomex-zip-codes , por lo que los id y los nombres no corresponden a los mismos que devuelve la API con la que se debe comparar.
Se ha respetado la estructura del json

## Instalar la API

```
git clone git@gitlab.com:azaelkaos/retotecnicobackbone.git
cd retotecnicobackbone
php artisan migrate
php artisan serve
```

## Nodos disponibles
para obtener los asentamientos por estado y localidad
api/zip-codes/Quintana Roo/Bacalar

para obtener los asentamientos por estado
api/zip-codes/Quintana Roo

para obtener los asentamientos por CP
api/zip-codes/77500 