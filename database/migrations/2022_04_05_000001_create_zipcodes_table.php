<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateZipcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zipcodes', function (Blueprint $table) {
            $table->id();
			$table->integer('idEstado')->nullable(false); 
			$table->string('estado')->nullable();
			$table->integer('idMunicipio')->nullable(false); 
			$table->string('municipio')->nullable();
			$table->string('ciudad')->nullable();
			$table->string('zona')->nullable();
			$table->integer('cp')->nullable(false); 
			$table->string('asentamiento')->nullable();
			$table->string('tipo')->nullable();
        });
		
		DB::unprepared(file_get_contents(base_path('database/seeders/sql/sepomex_abril-2016.sql')));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zipcodes');
    }
}
